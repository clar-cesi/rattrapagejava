package com.cl.buisness.ctrl;

import com.cl.cad.EmployeTbl;
import com.cl.cad.ParseDomXml;
import com.cl.cad.SaveXML;


/**
 * Created by cda09d on 22/09/2016.
 */
public class CtrlXml {

    private ConvertData cvd=new ConvertData();
    private EmployeTbl tblEmploye=new EmployeTbl();

    /** ouvre un fichier XML et le parse et retourne un String[] */
    public String[] read(String PathFileXml){
        return  cvd.setDataListEmploye(new ParseDomXml(PathFileXml).parse());
    }

    /** ecrire le contenu de la base.employe dans un fichier XML */
    public void write(){
        new SaveXML(tblEmploye.getAll());
    }

} /** fin class CtrlXml */
