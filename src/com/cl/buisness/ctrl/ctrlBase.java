package com.cl.buisness.ctrl;


import com.cl.cad.EmployeTbl;
import com.cl.cad.ParseDomXml;

/**
 * Created by cda09d on 22/09/2016.
 */
public class CtrlBase {

    private EmployeTbl tblEmploye=new EmployeTbl();
    private ConvertData cvd=new ConvertData();


    /** ecrire le contenu du fichier xml dans la base */
    public void write(String PathXml){
        tblEmploye.addAll(new ParseDomXml(PathXml).parse());
    }
    /** lire le contenu de la table retourne un arrayList */
    public String[] read(){
        return cvd.setDataListEmploye(tblEmploye.getAll());
    }
    /** vide (truncate) sur la table passé en parametre */
    public void removeAll(){ tblEmploye.removeAll("employe"); }
    /** supprime un enregistrement sur la table passé en parametre */
    public void remove(Integer val){tblEmploye.remove("employe",val); }

}/** fin class CtrlBase */
