package com.cl.buisness.entities;

/**
 * Created by cda09d on 19/09/2016.
 */
public class ModelEmploye {

    private String Nom;
    private String Adresse;
    private Integer Id;
    private Integer Age;
    private Integer Number;


    public ModelEmploye(){
        super(); // appel le constructeur parent
    }

    //constructeur
    public ModelEmploye(String nom, String adresse, Integer id, Integer age,Integer number) {
        Nom = nom;
        Adresse = adresse;
        Id = id;
        Age = age;
        Number = number;
    }


//getter setter

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public String getAdresse() {
        return Adresse;
    }

    public void setAdresse(String adresse) {
        Adresse = adresse;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Integer getAge() {
        return Age;
    }

    public void setAge(Integer age) {
        Age = age;
    }

    public Integer getNumber() {
        return Number;
    }

    public void setNumber(Integer number) {
        Number = number;
    }


    public String getLineSetDB(){
        return "('" + Nom + "','" + Adresse + "'," + Age + "," + Number + ")";
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(getNom());
        sb.append(" - " + getAdresse());
        sb.append(" - " + getNumber());
        sb.append(" - " + getAge());
        return sb.toString();
    }


}
