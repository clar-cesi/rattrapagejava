package com.cl.ihm;

import javax.swing.*;

/**
 * Created by cda09d on 20/09/2016.
 * Ouvre une boite de dialogue de choix de fichier
 */
public class OpenBoxFile {
    private JFileChooser dialogue;

    // ouvre une boite de dialogue de choix de fichier
    public OpenBoxFile() {
        dialogue = new JFileChooser();
        dialogue.showOpenDialog(null);
    }

    // retourn le path du fichier choisi
    public String getFilePath() {
        return dialogue.getSelectedFile().getPath();
    }

}
