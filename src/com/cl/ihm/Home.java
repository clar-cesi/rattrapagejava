package com.cl.ihm;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.cl.buisness.ctrl.ConvertData;
import com.cl.buisness.ctrl.CtrlBase;
import com.cl.buisness.ctrl.CtrlXml;
import com.cl.buisness.tools.MessageBox;


/**
 * Created by cda09d on 19/09/2016.
 */
public class Home extends Container {
    private JButton ouvirButton;
    private JList list1;
    private JButton infoButton;
    public JPanel panel1;
    private JList list2;
    private JButton button1Add;
    private JButton button2Cancel;
    private JComboBox comboBox1;
    private JLabel label1;
    private JTextArea textArea1;


    /** donnees locales */
    private String[] listInit={" "};
    private String[] listEmploye;
    private String[] listChoose;
    private Integer indexAdd=0;
    private String pathXmlSrc="D:\\Mes documents\\_A\\test2.xml";

    /** Controllers */
    private CtrlBase base=new CtrlBase();
    private CtrlXml xml=new CtrlXml();
    private ConvertData cvd=new ConvertData();
    private MessageBox mess=new MessageBox();

    /**  jtable*/
    //Les titres des colonnes
   private Object[][] dataTable = {
            {"Cysboy", "28 ans", "1.80 m"},
            {"BZHHydde", "28 ans", "1.80 m"},
            {"IamBow", "24 ans", "1.90 m"},
            {"FunMan", "32 ans", "1.85 m"}
    };
    String  title[] = {"Pseudo", "Age", "Taille"};
    JTable tableau = new JTable(dataTable, title);
   //panel1.getContentPane().add(new JScrollPane(tableau));


    public Home() {



       initData();
        // gesion des event sur la combobox menu
        comboBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String choixMenu = comboBox1.getSelectedItem().toString();
                System.out.println(" Menu index= >" + comboBox1.getSelectedIndex());
                switch(choixMenu)
                {
                    case "Open xml" : //ouvre un fichier XML et le parse
                        System.out.println("open xml");
                        pathXmlSrc=new OpenBoxFile().getFilePath();
                        listEmploye=xml.read(pathXmlSrc);
                        listChoose= new String[listEmploye.length*3];
                        list1.setListData(listEmploye);
                        list2.setListData(listChoose);
                        mess.display("Le fichier " + pathXmlSrc + " est ouvert ");
                        break;
                    case "Open base" :
                        System.out.println("open base");


                        list1.setListData(listInit);
                        list1.setListData( base.read());
                        mess.display("La table employee  est ouverte ");


                        break;
                    case "Export en xml" :
                        xml.write();
                        mess.display("La table employee  " + pathXmlSrc + " est exportée dans le fichier exportEmploye.xml  ");
                        break;
                    case "Save en base" :
                        base.write(pathXmlSrc);
                        mess.display("Le fichier " + pathXmlSrc + " est sauvegardé dans la table employee  ");
                        break;
                    case "Supprimer ligne base employe" : base.remove(2);   break;
                    case "Vider base employe" :
                        base.removeAll();
                        mess.display("La table employee a été vidée");
                        break;
                    case "Quitter":                       System.exit(0);   break;
                    default : break;
                }

            }
        });


        // ajouter un element a la liste de droite
        button1Add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                listChoose[indexAdd]=list1.getSelectedValue().toString();
                indexAdd++;
                list2.setListData(listChoose);


                //JOptionPane.showMessageDialog(null,"Ligne  " + list1.getSelectedIndex() + " => " + list1.getSelectedValue());

            }
        });
        // enlever un element a la liste de gauche
        button2Cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("=>" + list2.getSelectedIndex());
                listChoose[list2.getSelectedIndex()]=" ";
               // JOptionPane.showMessageDialog(null,"Ligne  " + list2.getSelectedIndex() + " => " + list2.getSelectedValue());

                list2.setListData(cvd.sortlist(listChoose));
            }
        });


    }



    public void initData(){
        //remplir le combobox
        String[] ListMenu={"Fichier","Open xml", "Open base", "Export en xml", "Save en base","Supprimer ligne base employe","Vider base employe","Quitter"};
        for(int i = 0 ; i < ListMenu.length;i++) {
            comboBox1.addItem(ListMenu[i]);
        }
    }

    public static void main(String[] args) {
        JFrame fn = new JFrame();
       fn.setTitle("Java TP1");
        fn.setLocation(200,100); // positionne la fenetre en x,
        fn.setSize(1000,900);
        fn.setLocationRelativeTo(null); // centre la fenetre

        fn.setContentPane(new Home().panel1);
        fn.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);






        fn.pack();
        fn.setVisible(true);
        System.out.println("endMain");





    }

}

// a faire
// exeption si pas fichier xml
// exeption si superieur au nombre prevu dans la list2
// sauvegarer en base ou en xml ou en fichier
// tpjava  employe  root/   127.0.0.1

// afficher dans un jtable

/*
    p3.add(new JScrollPane(tableau));
    p3 c mon panel
    tableau c mon jtable
    du coup j'ai 3 panel
    un grand qui est découpé en 2
 */
