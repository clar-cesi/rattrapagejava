package com.cl.ihm;

import javax.swing.*;
import java.awt.*;

/**
 * Created by cda09d on 25/09/2016.
 */
public class PanPanel extends JPanel {

    private int posX = -50;
    private int posY = -50;

    public void paintComponent(Graphics g){
        /* laissse une trainee
        g.setColor(Color.red);
        g.fillOval(posX, posY, 50, 50);*/
        //efface l'ancien rond
        g.setColor(Color.white);
        //On le dessine de sorte qu'il occupe toute la surface
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        //On redéfinit une couleur pour le rond
        g.setColor(Color.red);
        //On le dessine aux coordonnées souhaitées
        g.fillOval(posX, posY, 50, 50);

    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public void paintComponent1(Graphics g){
        //Vous verrez cette phrase chaque fois que la méthode sera invoquée
        this.setBackground(Color.red);
        System.out.println("Je suis exécutée !");


        int x1 = this.getWidth()/4;
        int y1 = this.getHeight()/4;
        //g.fillOval(x1, y1, this.getWidth()/2, this.getHeight()/2);
        //g.fillOval(20, 20, 75, 75);
       // g.drawRect(10, 10, 50, 60);
       // g.fillRect(65, 65, 30, 40);
       // g.drawRoundRect(10, 10, 30, 50, 10, 10);
       // g.fillRoundRect(55, 65, 55, 30, 5, 5);
        //g.drawLine(0, 0, this.getWidth(), this.getHeight());
        //g.drawLine(0, this.getHeight(), this.getWidth(), 0);
      /*  Font font = new Font("Courier", Font.BOLD, 20);
        g.setFont(font);
        g.setColor(Color.red);*/
        //g.drawString("Tiens ! Le Site du Zéro !", 10, 20);

   /*
    try {
      Image img = ImageIO.read(new File("images.jpg"));
      g.drawImage(img, 0, 0, this);
      //Pour une image de fond
      //g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
    } catch (IOException e) {
      e.printStackTrace();
    }

   */

/*
        Graphics2D g2d = (Graphics2D)g;
        GradientPaint gp, gp2, gp3, gp4, gp5, gp6;
        gp = new GradientPaint(0, 0, Color.RED, 20, 0, Color.magenta, true);
        gp2 = new GradientPaint(20, 0, Color.magenta, 40, 0, Color.blue, true);
        gp3 = new GradientPaint(40, 0, Color.blue, 60, 0, Color.green, true);
        gp4 = new GradientPaint(60, 0, Color.green, 80, 0, Color.yellow, true);
        gp5 = new GradientPaint(80, 0, Color.yellow, 100, 0, Color.orange, true);
        gp6 = new GradientPaint(100, 0, Color.orange, 120, 0, Color.red, true);

        g2d.setPaint(gp);
        g2d.fillRect(0, 0, 20, this.getHeight());
        g2d.setPaint(gp2);
        g2d.fillRect(20, 0, 20, this.getHeight());
        g2d.setPaint(gp3);
        g2d.fillRect(40, 0, 20, this.getHeight());
        g2d.setPaint(gp4);
        g2d.fillRect(60, 0, 20, this.getHeight());
        g2d.setPaint(gp5);
        g2d.fillRect(80, 0, 20, this.getHeight());
        g2d.setPaint(gp6);
        g2d.fillRect(100, 0, 40, this.getHeight());
*/

    }




}
