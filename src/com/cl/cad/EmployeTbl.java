package com.cl.cad;

import com.cl.buisness.entities.ModelEmploye;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cda09d on 20/09/2016.
 */
public class EmployeTbl extends ConnectDB {
    String tbl="employe";
    String chpAdd="Nom,Adresse,Age,numero";
    private List<ModelEmploye>  result= new ArrayList<>();

    // appel le constructeur de la classe mere
    public EmployeTbl() {super();}

    /**
     * select sur tbl
     * //@return List<ModelEmploye>
     */
    public List<ModelEmploye>  getAll(){
        String q="SELECT * FROM "+tbl;
        try {
            st=cnx.createStatement();
            rs=st.executeQuery(q);
            result.clear();
            if (rs==null)  {
                result.add(new ModelEmploye(" La base est vide","",0,0,0));
            }
            else {
                while(rs.next()) {
                    result.add( new ModelEmploye( rs.getString("Nom"),rs.getString("Adresse"),rs.getInt("id"), rs.getInt("Age"), rs.getInt("numero") )  );
                }
            }
            st.close();
            rs.close();
        }catch (SQLException e1) {
            e1.printStackTrace();
            System.out.println(e1.getMessage());
        }
        return result;
    }
    /**
     * insert en base tous les ModelEmploye conteneu dans la  List<ModelEmploye>
     * @param List<ModelEmploye>
     */
    public void addAll(List<ModelEmploye> r){
        String lines="";
        int nb=r.size();
        for(int i = 0 ; i < nb;i++){
            lines+=r.get(i).getLineSetDB();
           if (i<nb-1) lines+=",";
        }
        execQuerySet(" INSERT INTO "+tbl+" ("+ chpAdd+") VALUES "+lines+";");
    }
    /**
     * insert sur tbl
     * //@param empEl //" 'NomAdd', 'adrbidon', '52', '66666' ";
     * //@return ModelEmploye
     */
    public void add(String valP){
        execQuerySet(" INSERT INTO "+tbl+" ("+ chpAdd+") VALUES (NULL, " + valP +")");
    }

} /**fin classe  EmployeTbl*/
