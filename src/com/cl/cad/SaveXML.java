package com.cl.cad;


import java.io.File;
import java.io.FileOutputStream;
import java.util.Iterator;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import org.w3c.dom.Text;
import com.cl.buisness.entities.ModelEmploye;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;










//For jdk1.5 with built in xerces parser
//import com.sun.org.apache.xml.internal.serialize.OutputFormat;
//import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

//For JDK 1.3 or JDK 1.4  with xerces 2.7.1
//import org.apache.xml.serialize.XMLSerializer;
//import org.apache.xml.serialize.OutputFormat;

/**
 * Created by cda09d on 21/09/2016.
 * http://www.java-samples.com/showtutorial.php?tutorialid=152
 */
public class SaveXML {
    //No generics
    List myData;
    Document dom;
    private List<ModelEmploye>  result= new ArrayList<>();

    public SaveXML(List r) {
       // myData = new ArrayList();  //create a list to hold the data
        myData = r;  //create a list to hold the data
       // loadData();       //initialize the list
        createDocument(); //Get a DOM object
        System.out.println("Started .. ");
        createDOMTree();
        printToFile();
        System.out.println("Generated file successfully.");

    }

    /*public static void main(String args[]) {
        SaveXML xce = new SaveXML();
        xce.runExample();
    }*/

    /*public void runExample(){
        System.out.println("Started .. ");
        createDOMTree();
        printToFile();
        System.out.println("Generated file successfully.");
    }*/

    /**
     * Add a list of books to the list
     * In a production system you might populate the list from a DB
     */
    private void loadData(){
        myData.add(new ModelEmploye("NomExport1", "adresseExport1",1,25,5847));
        myData.add(new ModelEmploye("NomExport2", "adresseExport2",2,28,9988));

    }

    /**
     * Using JAXP in implementation independent manner create a document object
     * using which we create a xml tree in memory
    */
    private void createDocument() {
        //get an instance of factory

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            //get an instance of builder
            System.out.println("v1");
            DocumentBuilder db = dbf.newDocumentBuilder();
            //create an instance of DOM
            dom = db.newDocument();
        }catch(ParserConfigurationException pce) {
            //dump it
            System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
            System.exit(1);
        }

    }

    /**
     * The real workhorse which creates the XML structure
     */
    private void createDOMTree(){
        //create the root element <Personnel>
        Element rootEle = dom.createElement("Personnel");
        dom.appendChild(rootEle);
        //No enhanced for
        Iterator it  = myData.iterator();
        while(it.hasNext()) {
            ModelEmploye b = (ModelEmploye)it.next();
            //For each Book object  create <Book> element and attach it to root
            Element bookEle = createElement(b);
            rootEle.appendChild(bookEle);
        }
    }

    /**
     * Helper method which creates a XML element <Book>
     * @param b The book for which we need to create an xml representation
     * @return XML element snippet representing a book
     */
    private Element createElement(ModelEmploye b){
        System.out.println("v1");
        Element EmployeEle = dom.createElement("exportEmployee");
        EmployeEle.setAttribute("Type", "NA");

        //create name element and name text node and attach it to EmployeElement
        Element nameEle = dom.createElement("Name");
        Text nameText = dom.createTextNode(b.getNom());
        nameEle.appendChild(nameText);
        EmployeEle.appendChild(nameEle);

        //create adresse element and adresse text node and attach it to EmployeElement
        Element adresseEle = dom.createElement("Adresse");
        Text adresseText = dom.createTextNode(b.getAdresse());
        adresseEle.appendChild(adresseText);
        EmployeEle.appendChild(adresseEle);

        //create id element and id text node and attach it to EmployeElement
        Element idEle = dom.createElement("Id");
        Text idText = dom.createTextNode(b.getNumber().toString());
        idEle.appendChild(idText);
        EmployeEle.appendChild(idEle);

        //create age element and age text node and attach it to EmployeElement
        Element ageEle = dom.createElement("Age");
        Text ageText = dom.createTextNode(b.getAge().toString());
        ageEle.appendChild(ageText);
        EmployeEle.appendChild(ageEle);

        return EmployeEle;
    }

    /**
     * This method uses Xerces specific classes
     * prints the XML document to file.
     */
    private void printToFile(){

        try
        {
            //print
            OutputFormat format = new OutputFormat(dom);
            format.setIndenting(true);
            //to generate output to console use this serializer
            //XMLSerializer serializer = new XMLSerializer(System.out, format);
            //to generate a file output use fileoutputstream instead of system.out
            XMLSerializer serializer = new XMLSerializer(
                    new FileOutputStream(new File("Employe.xml")), format);
            serializer.serialize(dom);
        } catch(IOException ie) {
            ie.printStackTrace();
        }
    }





}
