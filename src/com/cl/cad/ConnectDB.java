package com.cl.cad;
import java.sql.*;
/**
 * Created by cda09d on 20/09/2016.
 * driver jdbc dispo :
 * http://www.java2s.com/Code/Jar/c/Downloadcommysqljdbc515jar.htm
 * *tuto https://www.youtube.com/watch?v=54mszeznwtc
 * http://www.objis.com/formation-java/tutoriel-java-acces-donnees-jdbc.html
 */
public class ConnectDB {
    Connection cnx=null;
    Statement st=null;
    ResultSet rs=null;

    public ConnectDB() {connecterDB(); }

        public  void connecterDB(){
        try {
            // chemin du driver
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("driver ok");
            String url="jdbc:mysql://localhost:3306/tpjava";
            String user="root";
            String pass="";
            cnx=DriverManager.getConnection(url,user,pass);
            System.out.println("Connection OK");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /** remove by id sur tbl  */
    public void remove(String tbl,int id){execQuerySet(" DELETE FROM "+tbl+" WHERE id = " + id);}

    /** truncate sur tbl */
    public void removeAll(String tbl){ execQuerySet(" TRUNCATE TABLE " + tbl); }

    /** execute une query sans retour de valeur */
    public void execQuerySet(String q){
        System.out.println(q);
        try {
            st=cnx.createStatement();
            st.executeUpdate(q);
            st.close();
        }catch (SQLException e1) {
            e1.printStackTrace();
            System.out.println(e1.getMessage());
        }
    }

}/**fin classe  ConnectDB*/
