package com.cl.cad;

import com.cl.buisness.entities.ModelEmploye;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by cda09d on 21/09/2016.
 * http://www.java-samples.com/showtutorial.php?tutorialid=152
 */
public class ParseDomXml {
    //get the factory
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    Document dom;
    //List myEmpls=new ArrayList();
    private List<ModelEmploye>  myEmplsL= new ArrayList<>();

    String pathXmlFile="D:\\Mes documents\\_A\\test2.xml";

    public ParseDomXml(String pathXmlFile) {
        this.pathXmlFile = pathXmlFile;
        System.out.println("pathXmlFile => " + pathXmlFile);
    }


    //public List parse(){
    public List<ModelEmploye>  parse(){
        parseXmlFile();
        parseDocument();
        //return myEmpls;
        return myEmplsL;
    }

    ///////////////////////////////////////////////
    // Get a document builder using document builder factory and parse the xml file to create a DOM object
    // recuperer un document builder qui utilise la factory builder et parse le xml dans on objet DOM
    /////////////////////////////////////////////
    private void parseXmlFile(){
        //get the factory
        //get the factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            System.out.println(" ");
            //Using factory get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
            //parse using builder to get DOM representation of the XML file
            dom = db.parse(pathXmlFile);
            System.out.println(dom);
        }catch(ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch(IOException ioe) {
            ioe.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }



    //Get a list of employee elements
    //Get the rootElement from the DOM object.From the root element get all employee elements.
    // Iterate through each employee element to load the data.
    private void parseDocument(){
        Element docEle = dom.getDocumentElement(); //get the root element
        NodeList nl = docEle.getElementsByTagName("Employee"); //get a nodelist of elements
        if(nl != null && nl.getLength() > 0) {
            for(int i = 0 ; i < nl.getLength();i++) {
                Element el = (Element)nl.item(i); //get the employee element
                //myEmpls.add(getEmployee(el)); //add it to list
                myEmplsL.add(getEmployee(el));
            }
        }
    }



    /**
     * I take an employee element and read the values in, create
     * an Employee object and return it
     * @param empEl
     * @return
     */
    private ModelEmploye getEmployee(Element empEl) {
        String name = getTextValue(empEl,"Name");
        String adresse = getTextValue(empEl,"Adresse");
        int id = 0;
        int age = getIntValue(empEl,"Age");
        int number = getIntValue(empEl,"Id");
        String type = empEl.getAttribute("type");

        return new ModelEmploye(name,adresse,id,age,number);
    }


    /**
     * I take a xml element and the tag name, look for the tag and get
     * the text content
     * i.e for <employee><name>John</name></employee> xml snippet if
     * the Element points to employee node and tagName is name I will return John
     * @param ele
     * @param tagName
     * @return
     */
    private String getTextValue(Element ele, String tagName) {
        String textVal = null;
        NodeList nl = ele.getElementsByTagName(tagName);
        if(nl != null && nl.getLength() > 0) {
            Element el = (Element)nl.item(0);
            textVal = el.getFirstChild().getNodeValue();
        }

        return textVal;
    }



    /**
     * Calls getTextValue and returns a int value
     * @param ele
     * @param tagName
     * @return
     */
    private int getIntValue(Element ele, String tagName) {
        //in production application you would catch the exception
        return Integer.parseInt(getTextValue(ele,tagName));
    }


}
